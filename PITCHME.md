# Wifi HTML & CSS 

Einführung in HTML & CSS für Webdesigner

---

### Überblick

- Einführung
- HTML
- CSS

---

### HTML

- Dispatcher: Manages Data Flow
- Stores: Handle State & Logic
- Views: Render Data via React

---

### CSS

- Dispatcher: Manages Data Flow
- Stores: Handle State & Logic
- Views: Render Data via React
